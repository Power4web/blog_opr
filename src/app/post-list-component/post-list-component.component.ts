import { Component, Input, OnInit } from '@angular/core';


@Component({
  selector: 'app-post-list-component',
  templateUrl: './post-list-component.component.html',
  styleUrls: ['./post-list-component.component.scss']
})
export class PostListComponentComponent implements OnInit {

  constructor() { }

  @Input() postDate: Date;
  @Input() postTitle: string;
  @Input() postContent: string;
  @Input() postLoveits: number;

  ngOnInit() {
  }

  vote(vote:number){
    this.postLoveits += vote;
  }

}
