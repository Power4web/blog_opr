import { Component } from '@angular/core';
import { Post } from './post';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  posts: Post[] = [
    {
      title: 'Mon premier post',
      content: 'Mon première article',
      loveits: -2,
      created_at: new Date()
    },
    {
      title: 'Mon deuxième post',
      content: 'Je te raconterais bien une histoire mais je manque d\'inspiration',
      loveits: 0,
      created_at: new Date()
    },
    {
      title: 'Encore un post',
      content: 'Si tu y tiens en voici un autre !',
      loveits: 1,
      created_at: new Date()
    }
  ];

  constructor() {

  }

}
