export class Post {
  title: string;
  content: string;
  loveits: number;
  created_at: Date;
}
